#!/bin/bash
project=$(basename -s .git $(git remote get-url origin))
echo $project
rawBranch=$(git rev-parse --abbrev-ref HEAD)
echo $rawBranch
org=$(git config --get remote.origin.url | grep -Eo "(\w*)/$project.git\/?$" | cut -d/ -f1)
echo $org
branch=$(python3 -c "import urllib.parse; print(urllib.parse.quote_plus('''$rawBranch'''))")
echo $branch
if [[ $org = *[!\ ]* ]]
then
    echo "Opening browser..."
else
    echo "Not in a bitbucket repo"
    exit
fi

open "https://bitbucket.org/${org}/${project}/pull-requests/new?source=${branch}"